<style>
    /* Warna latar belakang dan font yang sesuai */
    body {
        background-color: #555555 ;
        font-family: Arial, sans-serif;
    }

    .about-me-card {
        background-color: #555555  ;
        border-radius: 10px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
    }
</style>
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="about-me-card p-4">
                    <div class="row">
                        <!-- Kolom untuk gambar -->
                        <div class="col-md-4">
                            <img src="<?= BASEURL; ?>/img/fotoformal.jpeg" class="" style="object-fit:contain;" height="300px" width="300px" alt="">
                        </div>
                        <!-- Kolom untuk deskripsi -->
                        <div class="col-md-8 text-white">
                            <h2>About Me</h2>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde hic, repellendus necessitatibus porro soluta odio itaque! Officiis, minima quaerat dolores deleniti libero veniam placeat sit minus eum rerum quia vitae!
                            </p>
                            <p>Nama : I Putu Kona Arya Pratama Yasa <br>
                                Tempat/Tanggal Lahir : Denpasar, 15 Agustus 2006 <br>
                                Alamat Rumah : Jl. Gunung Agung Br. Merthayasa Gg. VII No. 7C <br>
                                Email : konaarya@gmail.com <br>
                                Phone : 085738314423
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>